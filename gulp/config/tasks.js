module.exports = [
  './gulp/tasks/fonts',
  './gulp/tasks/img',
  './gulp/tasks/pug',
  './gulp/tasks/sass',
  './gulp/tasks/scripts',
  './gulp/tasks/serve',
  './gulp/tasks/watch'
];
module.exports = function() {
  $.gulp.task('sass', function () {
      return $.gulp.src('src/static/sass/main.sass')
        .pipe($.gp.plumber(
          { errorHandler: $.gp.notify.onError("Error: <%= error.message %>")},
          function(err) {
            console.log('Styles Task Error');
            console.log(err);
            this.emit('END')
          }
        ))
        .pipe($.gp.sourcemaps.init())
        .pipe($.gp.sass({
          'include css': true
        }))
        .pipe($.gp.autoprefixer({
          overrideBrowserslist: ['last 6 versions', 'ie 8']
        }))
        .on("error", $.gp.notify.onError({
          message: "Error: <%= error.message %>",
          title: "Styles error"
        }))
        .pipe($.gp.csso())
        .pipe($.gp.sourcemaps.write())
        .pipe($.gulp.dest('build/static/css/'))
        .pipe($.bs.reload({
          stream: true
      }));
  });
}
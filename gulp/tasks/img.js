module.exports = function () {
  $.gulp.task('img:dev', function () {
    return $.gulp.src('src/static/img/**/*')
      .pipe($.gulp.dest('build/static/img/'));
  });

  $.gulp.task('img:build', function () {
    return $.gulp.src('src/static/img/*.+(jpg|jpeg|gif|png)')
      .pipe($.gp.tinypng('bZXvWf6HzXSzsr0jcYqtKBbWMQnTgb6k'))
      .pipe($.gulp.dest('build/static/img/'));
  });
}

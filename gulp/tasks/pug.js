module.exports = function() {
  $.gulp.task('pug', function() {
    return $.gulp.src('src/pug/*.pug')
          .pipe($.gp.plumber(
            { errorHandler: $.gp.notify.onError("Error: <%= error.message %>")},
            function(err) {
              console.log('Pug Task Error');
              console.log(err);
              this.emit('END')
            }
          ))
          .pipe($.gp.pug({
            pretty: true
          }))
          .pipe($.gulp.dest('build'))
          .on('end', $.bs.reload);
  });
}
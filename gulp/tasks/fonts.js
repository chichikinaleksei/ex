module.exports = function () {
  $.gulp.task('fonts', function () {
    return $.gulp.src([
      // 'node_modules/font-awesome/fonts/*',
      'src/static/fonts/*'
    ])
      .pipe($.gulp.dest('build/static/fonts/'));
  });
}
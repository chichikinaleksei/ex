module.exports = function() {
  $.gulp.task('scripts:lib', function () {
      return $.gulp.src([
        'node_modules/jquery/dist/jquery.min.js',
        'node_modules/slick-carousel/slick/slick.min.js',
        'node_modules/parallax-js/dist/parallax.js',
      ])
        .pipe($.gp.plumber(
          { errorHandler: $.gp.notify.onError("Error: <%= error.message %>")},
          function(err) {
            console.log('Scripts Task Error');
            console.log(err);
            this.emit('END')
          }
        ))
        .pipe($.gp.concat('libs.min.js'))
        .pipe($.gulp.dest('build/static/js/'))
        .pipe($.bs.reload({
          stream: true
      }));
  });
  
  $.gulp.task('scripts', function () {
      return $.gulp.src('src/static/js/main.js')
        .pipe($.gu())
        .pipe($.gulp.dest('build/static/js/'))
        .pipe($.bs.reload({
          stream: true
      }));
  });
}
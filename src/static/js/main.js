$(document).ready(function() {

  // Burger + menu animation
  $('.burger--main').on('click', () => {
    $('.navigation__wrapper--main').toggleClass('navigation__wrapper--is-closed');
    $('.burger__line--top').toggleClass('burger__line--top-is-rotated');
    $('.burger__line--middle').toggleClass('burger__line--middle-is-moved');
    $('.burger__line--bottom').toggleClass('burger__line--bottom-is-rotated');
    $('body').toggleClass('is-fixed');
    $('.burger__line--white').toggleClass('burger__line--black');
    $('.navigation').toggleClass('navigation--is-white');
    $('.logo__img--white').toggleClass('is-hidden');
    $('.logo__img--black').toggleClass('is-hidden');
  });
  $('.burger--fixed').on('click', () => {
    $('.navigation__wrapper--fixed').toggleClass('navigation__wrapper--is-closed');
    $('.burger__line--top').toggleClass('burger__line--top-is-rotated');
    $('.burger__line--middle').toggleClass('burger__line--middle-is-moved');
    $('.burger__line--bottom').toggleClass('burger__line--bottom-is-rotated');
    $('body').toggleClass('is-fixed');
    $('.burger__line--white').toggleClass('burger__line--black');
    $('.navigation').toggleClass('navigation--is-white');
    $('.logo__img--white').toggleClass('is-hidden');
    $('.logo__img--black').toggleClass('is-hidden');
  });

  // Navigation show
  if (window.matchMedia("(max-width: 768px)").matches) {

    let navigation = $('.navigation--fixed');
    let targetPos = 600;

    $(window).resize(() => {
      targetPos = $('.navigation-init').offset().top;
    })
    
    $(window).scroll(() => {
      let scrollPos = $(this).scrollTop();
  
      if(scrollPos > targetPos) {
        navigation.css({
          "opacity": "1",
          "transform": "translateY(0)"
        });
      } else if(scrollPos < targetPos) {
        navigation.css({
          "opacity": "0",
          "transform": "translateY(-100%)"
        });
      }
    });
  };



  // SLIDERS____________________
  // Brands - index page
  $('.brands__slider').slick({
    prevArrow: $(".brands__left"),
    nextArrow: $(".brands__right"),
    slidesToShow: 5,
    slidesToScroll: 1,
    infinite: true,
    dots: false,
    speed: 150,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1
        }
      }
    ]
  });
  // Engage - index page  
  if (window.matchMedia("(max-width: 940px)").matches) {
    $('.engage__slider').slick({
      arrows: false,
      slidesToShow: 3,
      slidesToScroll: 1,
      swipeToSlide: true,
      variableWidth: true,
      centerMode: true,
      dots: false 
    });
  }
  // Experience - about page
  $('.experience__slider').slick({
    prevArrow: $('.experience__left'),
    nextArrow: $('.experience__right'),
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true 
  });
  // News slider
  $('.news-slider__slider').slick({
    prevArrow: $('.news-slider__arrow-left'),
    nextArrow: $('.news-slider__arrow-right'),
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: true 
  });
  // SLIDERS____________________

  // Engage choose
  $('.engage__item').on('click', function() {
    let itemAttr = $(this).data('engage-item');
    let photo = $('.engage__photo-wrapper[data-engage-photo="' + itemAttr + '"]');
    let panel = photo.next().prop('scrollHeight');
    

    $('.engage__item').removeClass('engage__item--active');
    $(this).addClass('engage__item--active');
    
    // $('.engage__photo-wrapper').hide(300);
    // photo.show(300);
    $('.engage__photo-wrapper').addClass('engage__photo-wrapper--is-hidden');
    setTimeout(() => {
      photo.removeClass('engage__photo-wrapper--is-hidden');
    },200);
  });

  // Form animation
  $("form :input").focus(function(e) {
    $('label[for="' + this.id + '"]').addClass("form__label-is-active")
  }).blur(function(e) {
    0 == $(this).val().length && ($('label[for="' + this.id + '"]').removeClass("form__label-is-active"),
    $("html").one("click", ()=>{
        $('label[for="' + this.id + '"]').removeClass("form__label-is-active");
    }),
    e.stopPropagation())
  });

  // CUSTOM SELECT
  $(".custom-select").each(function() {
    var classes = $(this).attr("class"),
        id      = $(this).attr("id"),
        name    = $(this).attr("name");
    var template =  '<div class="' + classes + '">';
        template += '<span class="custom-select-trigger">' + $(this).attr("placeholder") + '</span>';
        template += '<div class="custom-options">';
        $(this).find("option").each(function() {
          template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
        });
    template += '</div></div>';
    
    $(this).wrap('<div class="custom-select-wrapper"></div>');
    $(this).hide();
    $(this).after(template);
  });
  $(".custom-option:first-of-type").hover(function() {
    $(this).parents(".custom-options").addClass("option-hover");
  }, function() {
    $(this).parents(".custom-options").removeClass("option-hover");
  });
  $(".custom-select-trigger").on("click", function(event) {
    $('html').one('click',function() {
      $(".custom-select").removeClass("opened");
    });
    $(this).parents(".custom-select").toggleClass("opened");
    event.stopPropagation();
  });
  $(".custom-option").on("click", function() {
    $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
    $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
    $(this).addClass("selection");
    $(this).parents(".custom-select").removeClass("opened");
    $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
  });
  // CUSTOM SELECT end

  // Ajax load
  let data = {
		action: 'load-more'
  }
  $('.news__load-more').on('click', e => {
    e.preventDefault();
    jQuery.post( '/load-more.php', data, function(response) {
			alert('Success');
		});
	
  });


  // Fonts script
  let titles = document.querySelectorAll('.fs');
  titles.forEach(title => {
    let titleHtml = title.innerHTML;
    let dataRow = title.getAttribute('data-row');
    let dataSpeed = title.getAttribute('data-speed');
    let markup = `<span class="row zero-bottom" style="transition: all ${dataSpeed}ms ease"><i>${titleHtml}</i></span>`;
    let markupHtml = [];
    for (let i = 0; i < dataRow; i++) {
      markupHtml += markup;
    }
    title.insertAdjacentHTML('beforeend', markupHtml);
  });

  setTimeout(() => {
    $('.row').removeClass('zero-bottom');
  }, 1000);  
  
}); // jQuery end

let scene = document.getElementById('scene');
if(scene) {
  let parallaxInstance = new Parallax(scene);
}


